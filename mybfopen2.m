function [data, par] = mybfopen2(fileName)
%%%wrapper for bfopen to readImageData meta data structure
%%%is not tested yet..
%%%by Florian Vollrath @ 01.07.2016
%% import file with bfopen
result = bfopen(fileName);
%% meta data structure
% par.time.pixelT = NaN;
% par.time.lineT = NaN;
% par.time.frameT = NaN;
% par.time.stackT = NaN;
%% transform result into metadata structure
%meta data
ome = result{1,4};
%dimensions
try par.dim.x = double(ome.getPixelsSizeX(0).getValue()); catch par.dim.x = NaN; end
try par.dim.y = double(ome.getPixelsSizeY(0).getValue()); catch par.dim.y = NaN; end
try par.dim.z = double(ome.getPixelsSizeZ(0).getValue()); catch par.dim.z = NaN; end
try par.dim.t = double(ome.getPixelsSizeT(0).getValue()); catch par.dim.t = NaN; end
try par.dim.ch = double(ome.getPixelsSizeC(0).getValue()); catch par.dim.ch = NaN; end
try par.dim.type = char(ome.getPixelsType(0)); catch par.dim.type = 'uint16'; end;

%scales
try par.scale.x = double(ome.getPixelsPhysicalSizeX(0).value()); catch par.scale.x = NaN; end
try par.scale.y = double(ome.getPixelsPhysicalSizeY(0).value()); catch par.scale.y = NaN; end
try par.scale.z = double(ome.getPixelsPhysicalSizeZ(0).value()); catch par.scale.z = NaN; end

%scaling units
try par.scale.xUnit = char(ome.getPixelsPhysicalSizeY(0).unit().getSymbol()); catch par.scale.xUnit = 'Unknown'; end
try par.scale.yUnit = char(ome.getPixelsPhysicalSizeX(0).unit().getSymbol()); catch par.scale.yUnit = 'Unknown'; end
try par.scale.zUnit = char(ome.getPixelsPhysicalSizeZ(0).unit().getSymbol()); catch par.scale.zUnit = 'Unknown'; end

%objective properties
try par.immersionMedium = char(ome.getObjectiveImmersion(0,0)); catch par.immersionMedium = 'Unknown'; end
try par.objectiveNA = double(ome.getObjectiveLensNA(0,0)); catch par.objectiveNA = NaN; end
try par.objectiveName = char(ome.getObjectiveModel(0,0)); catch par.objectiveName = 'Unknown'; end
try par.refractiveIndex = double(ome.getObjectiveSettingsRefractiveIndex(0)); catch par.refractiveIndex = NaN; end
try par.objectiveMag = double(ome.getObjectiveNominalMagnification(0,0)); catch par.objectiveMag = NaN; end

%acquisition properties
try par.zoom = double(ome.getDetectorZoom(0, 0)); catch par.zoom = NaN; end
try par.gain = double(ome.getDetectorGain(0, 0)); catch par.gain = NaN; end

%laser properties
%exc and emission wavelengths
if par.dim.ch > 1
    for ch = 0:par.dim.ch-1
        try par.light.excWavelength(ch+1) = double(ome.getChannelExcitationWavelength(0,ch).value()); catch par.light.excWavelength(ch+1) = NaN; end
        try par.light.emWavelength(ch+1) = double(ome.getChannelEmissionWavelength(0,ch).value()); catch par.light.emWavelength(ch+1) = NaN; end
    end
else
    try par.light.excWavelength = double(ome.getChannelExcitationWavelength(0,0).value()); catch par.light.excWavelength = NaN; end
    try par.light.emWavelength = double(ome.getChannelEmissionWavelength(0,0).value()); catch par.light.emWavelength = NaN; end
end

%get time unit, deltaT is retrieved in next section
try dTUnit = char(ome.getPlaneDeltaT(0,1).unit().getSymbol()); catch dTUnit = nan; end
par.time.timeUnit = dTUnit;

%% import data
%check for the number of series
numPlanes = size(result{1},1);
numSeries = size(result,1);
%preallocate data
data = zeros(par.dim.x, par.dim.y, par.dim.ch, par.dim.z, par.dim.t, numSeries, par.dim.type);
par.time.timeStamps = zeros(1, par.dim.t, 'double');
%fill out data array
for s = 1:numSeries
    ome = result{s,4};
    for k=0:numPlanes-1
        %extract dimensions out of string
        try ch = ome.getPlaneTheC(s-1,k).getValue()+1; catch ch = 1; end
        try z = ome.getPlaneTheZ(s-1,k).getValue()+1; catch z = 1; end
        try t = ome.getPlaneTheT(s-1,k).getValue()+1; catch t = k+1; end
        
        %copy data
        data(:,:,ch,z,t, s) = permute(result{s}{k+1,1}, [2,1]);
        
        %clear data from results when it is copied
        result{s}{k+1,1} = [];
    end
end

%process time stamps
for t = 1:par.dim.t
    try dT = double(ome.getPlaneDeltaT(0,t-1).value()); catch dT = nan; end
    %save timestamps
    if ~isnan(dT)
        par.time.timeStamps(t) = dT;
    end
end
end