function [varargout] = profile2data(im,points,varargin)
  % Function to obtain data from a matrix along a defined profile
  % [profile, profile_Err, par, Ap, Ip] = ...
  %         profile2data(im, points, 'FunType','mean','Weight', A_i, 'Width', 5,'ScaleFactor', 1, 'verbose', 0 )

  profile2data_date='01.02.2015';
  profile2data_version='0.9';
  % 
  %% Input
  p = inputParser;
  p.StructExpand    = true;  % if we allow parameter  structure expanding
  %p.CaseSensitivity = false; % enables or disables case-sensitivity when matching entries in the argument list with argument names in the schema. Default, case-sensitive matching is disabled (false).
  p.KeepUnmatched   = true;  % controls whether MATLAB throws an error (false) or not (true) when the function being called is passed an argument that has not been defined in the inputParser schema for this file.
  p.FunctionName    = ['profile2data v.' profile2data_version ' (' profile2data_date ')']; % stores a function name that is to be included in error messages that might be thrown in the process of validating input arguments to the function.

  addRequired(p, 'im',     @checkim);
  addRequired(p, 'points', @checkpoints);
  addParameter(p,'FunType', 'mean',@checkFunType);
  addParameter(p,'Weight',   false,@checkWeight);
  addParameter(p,'Width',        5,@checkWidth);                              % profile width in um, when resolution is 1 um/pix, profile width is equivalent to pixel
  addParameter(p,'StepSize',     1,@checkScaleFactor);                        % profile step size in um, when resolution is 1 um/pix, profile step size is equivalent to pixel
  addParameter(p,'Resolution',   1,@checkScaleFactor);                        % pixel resolution in um per pixel
  p.addParameter('verbose'         , false, @(x)(islogical(x) || any(x==[0 1 2])));
  p.addParameter('gui'             , false, @(x)(islogical(x) || any(x==[0 1 2 3])));
  p.addParameter('timestamp'       , false, @(x)(islogical(x) || isnumeric(x)));
  parse(p,im,points,varargin{:});
  par = p.Results;
  clear im points;
  
  %% start timer for verbose and par.gui, TIC is global!!!
  % setting clock
  if par.verbose
    if ~par.timestamp
      par.timestamp = tic;
    end
    t1 = toc(par.timestamp);             % starts timer for verbose
  end
%   fprintf('\n')
  if par.verbose; t1 = output(sprintf('profile2data: Start profile2data v. %s (%s)!',...
      profile2data_version ,profile2data_date), whos, toc(par.timestamp), t1); end

  % add unmached structure elements
  s = fieldnames(p.Unmatched);
  for n=1:length(s)
    par.(s{n}) = p.Unmatched.(s{n}); % '.(s{n})' acts like setfield(par, s(1), getfield(p.Unmatched, s{1})
  end
  
  
  if nargout==0
    par.gui = true;
  end
  
  
  % define remaining parameters
  Algor = 'pchip';
  calErr = false; % Error calculation
  
  varargout = cell(1,1);
  if nargout>1 %&& ~istilde(2)
    calErr = true;
  end
  if nargout>6
    error('to many output variables!')
  end
  if par.verbose; t1 = output(sprintf('profile2data: Parameters defined'), whos, toc(par.timestamp), t1); end
  
  %% start evaluation
  A = single(par.im);
  par = rmfield(par,'im');
  if any(par.Weight(:))
    I = single(par.Weight);
    par.myWeight = true;
    par = rmfield(par,'Weight');
  else
    par.myWeight = false;
  end
  sz = size(A);
  if size(par.points,2)>2
    par.points = par.points';
  end
  
  %% move structure fields par -> pI
  fieldList = {'FunType','myWeight','Resolution','StepSize','Width','points'};
  for nn = 1:length(fieldList)
    pI.(fieldList{nn}) = par.(fieldList{nn});
  end
  par = rmfield(par,fieldList);
  pI.xStep = pI.StepSize/pI.Resolution; % path step size in pixel
  pI.wStep = pI.Width/pI.Resolution;    % path width (border to border) in pixel
  if par.verbose; t1 = output(sprintf('profile2data: Data extracted'), whos, toc(par.timestamp), t1); end
  
  
  %% Reshape Matrix
  if ndims(A)>3
    A = reshape(A,[sz(1:2) prod(sz(3:end))]);
    if pI.myWeight
      I = reshape(I,[sz(1:2) prod(sz(3:end))]);
    end
  end
  if par.verbose; t1 = output(sprintf('profile2data: Data reshaped'), whos, toc(par.timestamp), t1); end
  
  %% get points and Mat
  lW = ceil(pI.wStep);
  lS = ceil(pI.xStep);
  pI.smoothpoints = imgTraceSmooth(pI.points);
  pI.pointmat = imgTraceMatrix(pI.smoothpoints);
  if par.verbose; t1 = output(sprintf('profile2data: smoothed points and matrix calculated'), whos, toc(par.timestamp), t1); end
  
  %% Loop over dimensions
  pI.xvals = ((0:(size(pI.pointmat,2)-1))*pI.StepSize)';
  for n = 1:size(A,3)
    Ap(:,:,n) = interp2(double(A(:,:,n)),pI.pointmat(:,:,1),pI.pointmat(:,:,2));
  end
  if pI.myWeight
    for n = 1:size(A,3)
      Ip(:,:,n) = interp2(double(I(:,:,n)),pI.pointmat(:,:,1),pI.pointmat(:,:,2));
    end
  end
  
  if par.verbose; t1 = output(sprintf('profile2data: Loop over dimensions done'), whos, toc(par.timestamp), t1); end
  if calErr
    imgTraceProfile_Err;
  else
    imgTraceProfile;
  end
  if par.verbose; t1 = output(sprintf('profile2data: profiles calculated'), whos, toc(par.timestamp), t1); end
  
  
  %% Show Plot
  if par.gui == 5
    figure('Units','Pixels','Position',[100 100 800 800]);
    axes('Color','w');
    hold all
    colors = colorMaps(6,'MapSize',size(A,3));
    for n = 1:size(A,3)
      if calErr
        errorbar(pI.xvals,profile(:,n),profile_Err(:,n),'Color',colors(n,:))
      else
        plot(pI.xvals,profile(:,n),'Color',colors(n,:));
      end
    end
    title('Profile Plots from Data');
    xlabel('Distance');
    ylabel('Intensity');
    box on;
    if par.verbose; t1 = output(sprintf('profile2data: Profile figure generated '), whos, toc(par.timestamp), t1); end
  end
  
 
  %% create output
  if nargout > 0
    if length(sz)>3
      varargout{1} = reshape(profile,[size(profile,1),sz(3:end)]);
    else 
      varargout{1} = profile;
    end
  end
  if nargout > 1
    if calErr
      if length(sz)>3
        if strcmp(pI.FunType,'median')
          varargout{2} = reshape(profile_Err,[size(profile,1),size(profile,2),sz(3:end)]);
        else
          varargout{2} = reshape(profile_Err,[size(profile,1),sz(3:end)]);
        end
      else
        varargout{2} = profile_Err;
      end
    else
      varargout{2} = [];
    end
  end
  if nargout > 2
    pI.pointmat = reshape(pI.pointmat,[size(pI.pointmat,1)/lS,size(pI.pointmat,2)*lS,2]);
    varargout{3} = orderfields(pI,[1:5 7 8 6 9 10 11]);
  end
  if nargout > 3
    varargout{4} = par;
  end
  if nargout > 4
    varargout{5} = reshape(Ap,[size(Ap,1)/lS,size(Ap,2)*lS,sz(3:end)]);
  end
  if nargout > 5
    varargout{6} = reshape(Ip,[size(Ip,1)/lS,size(Ip,2)*lS,sz(3:end)]);
  end
  if par.verbose; t1 = output(sprintf('profile2data: output prepared, function DONE'), whos, toc(par.timestamp), t1); end
  
  
  %% Functions
  function smoothpoints = imgTraceSmooth(tracepoints)
    if numel(unique(tracepoints(:,1)))>1
      x = tracepoints(:,1);
      y = tracepoints(:,2);
      dist = [0; cumsum(sqrt((x(2:end)-x(1:end-1)).^2+(y(2:end)-y(1:end-1)).^2))];
      [~,ind,~] = unique(dist);
      dist = dist(ind);
      x = x(ind);
      y = y(ind);
      distt = (0:pI.xStep/lS:ceil(dist(end)))';
      xp = interp1(dist,x,distt,Algor);
      yp = interp1(dist,y,distt,Algor);
      dist = [0; cumsum(sqrt((xp(2:end)-xp(1:end-1)).^2+(yp(2:end)-yp(1:end-1)).^2))];
      distt = (0:pI.xStep/lS:ceil(dist(end)))';
      xp = interp1(dist,xp,distt,'linear','extrap');
      yp = interp1(dist,yp,distt,'linear','extrap');
      smoothpoints = [xp,yp];
    else
      smoothpoints = tracepoints;
    end
  end
  function perpendicularmat = imgTraceMatrix(tracepoints)
    xp = tracepoints(:,1);
    yp = tracepoints(:,2);
    % width of area to use for profile
    w = linspace(-(pI.wStep-1)/2,(pI.wStep-1)/2,lW); % width: |< border to border >|
    % length of profile
    T = length(xp);
    t = 1:T;
    % calc slope
    tp1 = max([t-1; ones(1,T)]);
    tp2 = min([t+1; T*ones(1,T)]);
    %p  = [xp(t) yp(t)];
    p1 = [xp(tp1) yp(tp1)];
    p2 = [xp(tp2) yp(tp2)];
    dp = p2-p1;
    % norm slope
    dp = dp./repmat(sqrt(dp(:,1).^2+dp(:,2).^2),[1 2]);
    % perform matrix reshaping to allow rotation
    dp = dp';
    dp = dp(:)*w;
    dp = reshape(dp', lW,2,[]);
    dp = reshape(permute(dp,[1 3 2]),[],2);
    dp = dp*[0 1;-1 0]; % rotation vector
    % reshaping for x and y position
    xi = reshape(dp(:,1),lW,[])+repmat(xp',[lW 1]);
    yi = reshape(dp(:,2),lW,[])+repmat(yp',[lW 1]);
    xsz = size(xi);
    xi = xi(:,1:(floor(size(xi,2)/lS)*lS));
    yi = yi(:,1:(floor(size(xi,2)/lS)*lS));
    xsz = size(xi);
    xi = reshape(xi,[xsz(1)*lS xsz(2)/lS]);
    yi = reshape(yi,[xsz(1)*lS xsz(2)/lS]);
    perpendicularmat = cat(3,xi,yi);
  end
  function imgTraceProfile
    switch pI.FunType
      case 'mean'
        if Weight
          profile = nansum(Ap.*Ip)./nansum(Ip);
        else
          profile = nanmean(Ap,1);
        end
      case 'median'
        if Weight
          profile = wmedian(Ap,Ip);
        else
          profile = nanmedian(Ap,1);
        end
      case 'sum'
        profile = nansum(Ap,1);
      case 'min'
        profile = nanmin(Ap,[],1);
      case 'max'
        profile = nanmax(Ap,[],1);
    end
    profile = squeeze(profile);
    if size(profile,1)==1
      profile = profile';
    end
  end % ANDRE
  function imgTraceProfile_Err
    switch pI.FunType
      case 'mean'
        if pI.myWeight
          profile = nansum(Ap.*isfinite(Ap).*Ip.*isfinite(Ap))./nansum(Ip.*isfinite(Ap)); % sum(Ap.*Ip)./sum(Ip);
          profile_Err = sqrt( nansum( isfinite(Ap).*(Ap - repmat(profile,[lW*lS 1])).^2 .* Ip.*isfinite(Ip))./nansum(Ip.*isfinite(Ip)))./sqrt(nansum(isfinite(Ap) & isfinite(Ip)));  %#ok % sem Intensity % Standard Error of mean
        else
          profile = nanmean(Ap,1);
          profile_Err = std(Ap);
        end
      case 'median'
        if pI.myWeight
          [profile, profile_Err] = wmedian(Ap,Ip);
        else
          profile = nanmedian(Ap,1);
          profile_Err = 2*1.58*(profile-prctile(Ap,25,1))./sqrt(sum(~isnan(Ap)));
          imdim = {2}; for ni = 2:ndims(profile_Err); imdim{ni} =':'; end 
          profile_Err(imdim{:}) = 2*1.58*(prctile(Ap,75,1)-profile)./sqrt(sum(~isnan(Ap)));
        end
      otherwise
        warning(sprintf('No error calculation possible for this function type\nCalculate profiles without error instead'))
    end
    profile = squeeze(profile);
    profile_Err = squeeze(profile_Err);
    if size(profile,1)==1
      profile     = profile';
      profile_Err = profile_Err';
    end
  end % ANDRE
  
  function out = checkim(val)
    out = 0;
    if ~isnumeric(val)
      error('Input im has to be numerical!');
    elseif ndims(val)<2
      error('Input im has to have at least 2 dimensions!');
    else
      out = 1;
    end
  end
  function out = checkpoints(val)
    out = 0;
    if ~ismatrix(val)
      error('Input points has to be a two dimensionsal matrix!');
    elseif any(size(val))==2
      error('Input points has to have one dimension with size of 2!');
    elseif any(val)<=0
      error('Input points have to be all positive integers!');
    else
      out = 1;
    end
  end
  function out = checkFunType(val)
    out = any(strcmpi(val,{'mean','median','sum','max','min'}));
%     switch (val)
%       case 'mean'
%         out = 1;
%       case 'sum'
%         out = 1;
%       case 'max'
%         out = 1;
%       case 'min'
%         out = 1;
%       otherwise
%         out = 0;
%     end
  end
  function out = checkWeight(val)
    out = 0;
    if  all(val(:) == false) || (ndims(val)==ndims(im) && all(size(val)==size(im))) %any(val(:) ~= 0)
      out = 1;
    else
      error('Weight Factor has to be FALSE or same size as Input im');
    end
  end
  function out = checkWidth(val)
    out = 0;
    if ~isscalar(val)
      error('Input Width has to be a scalar!')
    else
      out = 1;
    end
  end
  function out = checkScaleFactor(val)
    out = 0;
    if ~isscalar(val) || (val)<=0
      error('Input Width has to be a positive scalar!')
    else
      out = 1;
    end
  end
  function out = checkShowPlot(val)
    out = 0;
    if ~isscalar(val) && val~=0 || val~=1
      error('Input ShowPlot has to be convertible to logical!')
    else
      out = 1;
    end
  end
end